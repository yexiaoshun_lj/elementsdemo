package demo.jliu.elementsdemo.ui;

import android.app.DialogFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import demo.jliu.elementsdemo.R;
import demo.jliu.elementsdemo.mode.Item;
import demo.jliu.elementsdemo.net.Decoder;
import demo.jliu.elementsdemo.net.NetClient;
import demo.jliu.elementsdemo.util.IOUtil;

public class MainActivity extends AppCompatActivity implements FailedToFetchDataDialog.ButtonCallBack {
    public static final String KEY_ITEM_LIST = "item list";
    public static final String KEY_INVALID_LINES = "invalid lines";
    public static final String CSV_CONTENT_URL = "https://docs.google.com/spreadsheet/ccc?key=0Aqg9JQbnOwBwdEZFN2JKeldGZGFzUWVrNDBsczZxLUE&single=true&gid=0&output=csv";
    private RecyclerViewAdapter adapter;
    private ArrayList<Item> itemList;
    private ProgressBar progressBar;
    private ArrayList<String> invalidLines;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActionBar();

        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new RecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);

        if (savedInstanceState != null) {
            itemList = savedInstanceState.getParcelableArrayList(KEY_ITEM_LIST);
            invalidLines = savedInstanceState.getStringArrayList(KEY_INVALID_LINES);
        }

        if (itemList == null) {
            hideFailedToLoadDialogIfExists();
            loadData();
        }
    }

    private void hideFailedToLoadDialogIfExists() {
        DialogFragment dialogFragment = (DialogFragment) getFragmentManager().findFragmentByTag(FailedToFetchDataDialog.TAG);
        if (dialogFragment != null) {
            dialogFragment.dismiss();
        }
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(KEY_ITEM_LIST, itemList);
        outState.putStringArrayList(KEY_INVALID_LINES, invalidLines);
    }

    @Override
    public void onRetryClicked() {
        loadData();
    }

    private void loadData() {
        progressBar.setVisibility(View.VISIBLE);
        new LoadDataTask(this).execute();
    }

    private static class LoadDataTask extends AsyncTask<Void, ArrayList<String>, ArrayList<Item>> {
        private static final String TAG = "LoadDataTask";
        private WeakReference<MainActivity> contextRef;

        private LoadDataTask(MainActivity context) {
            contextRef = new WeakReference<>(context);
        }

        @Override
        protected ArrayList<Item> doInBackground(Void... params) {
            try {
                String raw = loadData();
                if (TextUtils.isEmpty(raw)) {
                    return null;
                }
                Decoder decoder = new Decoder(raw);
                decoder.decode();
                if (decoder.hasErrors()) {
                    publishProgress(decoder.getInvalidLines());
                }
                return decoder.getResult();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when downloading CSV file:" + e.getMessage());
            }
            return null;
        }

        private String loadData() throws IOException {
            MainActivity context = contextRef.get();
            if (context != null) {
                String data = IOUtil.readCacheFromFile(context);
                if (TextUtils.isEmpty(data)) {
                    data = NetClient.getString(CSV_CONTENT_URL);
                    IOUtil.saveCacheFile(data, context);
                    return data;
                } else {
                    return data;
                }
            } else {
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(ArrayList<String>... values) {
            MainActivity activity = contextRef.get();
            if (activity != null) {
                activity.showInvalidLines(values[0]);
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Item> items) {
            MainActivity activity = contextRef.get();
            if (activity != null) {
                activity.updateList(items);
            }
        }
    }

    private void showInvalidLines(ArrayList<String> value) {
        invalidLines = value;
        invalidateOptionsMenu();
    }

    private void updateList(ArrayList<Item> items) {
        if (items == null) {
            showFailedDialog();
        } else {
            adapter.setItems(items);
            adapter.notifyDataSetChanged();
        }
        progressBar.setVisibility(View.GONE);
    }

    private void showFailedDialog() {
        FailedToFetchDataDialog.show(getFragmentManager());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.actionbar_error).setVisible(invalidLines != null && !invalidLines.isEmpty());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionbar_error:
                showInvalidDialog();
                return true;
            case R.id.actionbar_clear_cache:
                clearCacheData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showInvalidDialog() {
        StringBuilder stringBuilder = new StringBuilder();
        for (String line : invalidLines) {
            stringBuilder.append(line).append("\n");
        }
        String msg = getString(R.string.invalid_lines_message, stringBuilder.toString());
        SimpleDialog.showDialog(getFragmentManager(), "", msg);
    }

    private void clearCacheData() {
        new ClearCacheTask().execute();
    }

    private class ClearCacheTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            IOUtil.clearCache(MainActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(MainActivity.this, R.string.cache_cleared, Toast.LENGTH_LONG).show();
        }
    }
}
