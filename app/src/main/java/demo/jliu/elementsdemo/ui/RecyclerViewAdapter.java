package demo.jliu.elementsdemo.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import demo.jliu.elementsdemo.mode.Item;
import demo.jliu.elementsdemo.util.LayoutUtil;
import demo.jliu.elementsdemo.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private ArrayList<Item> items;
    private Context context;
    private int imageWidth;
    private int imageHeight;

    public RecyclerViewAdapter(Context context) {
        this.context = context;
        imageWidth = (int) LayoutUtil.convertDpToPixel(context.getResources().getDimension(R.dimen.item_photo_width), context);
        imageHeight = (int) LayoutUtil.convertDpToPixel(context.getResources().getDimension(R.dimen.item_photo_height), context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item item = items.get(position);
        holder.title.setText(item.getTitle());
        holder.description.setText(item.getDescription());
        String imageUrl = item.getImageUrl();
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.item_error)
                    .resize(imageWidth, imageHeight)
                    .into(holder.image);
        } else {
            holder.image.setImageResource(R.drawable.item_error);
        }

        holder.itemView.setTag(item);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Item item = (Item) v.getTag();
                DetailActivity.start(context, item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView description;
        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
            image = (ImageView) itemView.findViewById(R.id.item_image);
        }
    }
}
