package demo.jliu.elementsdemo.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import demo.jliu.elementsdemo.R;
import demo.jliu.elementsdemo.mode.Item;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_ITEM = "item";

    public static void start(Context context, Item item) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_ITEM, item);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        setupActionbar();
        ImageView imageView = (ImageView) findViewById(R.id.item_image);
        TextView title = (TextView) findViewById(R.id.title);
        TextView description = (TextView) findViewById(R.id.description);

        Item item = getIntent().getParcelableExtra(EXTRA_ITEM);

        title.setText(item.getTitle());
        description.setText(item.getDescription());
        Picasso.with(this)
                .load(item.getImageUrl())
                .placeholder(R.drawable.loading)
                .error(R.drawable.item_error)
                .fit()
                .into(imageView);
    }

    private void setupActionbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
