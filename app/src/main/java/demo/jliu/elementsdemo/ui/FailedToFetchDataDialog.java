package demo.jliu.elementsdemo.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;

import demo.jliu.elementsdemo.R;

public class FailedToFetchDataDialog extends DialogFragment {

    public static final String TAG = "FailedToFetchDataDialog.TAG";

    public interface ButtonCallBack {
        void onRetryClicked();
    }

    public static void show(FragmentManager fm) {
        FailedToFetchDataDialog dialog = new FailedToFetchDataDialog();
        dialog.show(fm, TAG);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setMessage(R.string.failed_to_fetch_data)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ButtonCallBack callBack = (ButtonCallBack) getActivity();
                        callBack.onRetryClicked();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
    }
}
