package demo.jliu.elementsdemo.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class IOUtil {

    public static final String TAG = "IOUtil";
    public static final String FILE_CACHE_DATA = "cache_data";

    public static String readCacheFromFile(Context context) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(getCacheFile(context));
            return scanner.useDelimiter("\\A").next();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "error occurred when reading cache file:" + e.getMessage());
            return null;
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
    }

    @NonNull
    private static File getCacheFile(Context context) {
        File file = new File(context.getCacheDir(), FILE_CACHE_DATA);
        file.mkdirs();
        return file;
    }

    public static void clearCache(Context context) {
        getCacheFile(context).delete();
    }

    public static void saveCacheFile(String string, Context context) {
        FileOutputStream outputStream = null;

        try {
            File cacheFile = getCacheFile(context);
            if (cacheFile.exists()) {
                cacheFile.delete();
            }
            outputStream = new FileOutputStream(cacheFile);
            outputStream.write(string.getBytes());
        } catch (IOException e) {
            Log.e(TAG, "error occurred when writing cache file:" + e.getMessage());
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
