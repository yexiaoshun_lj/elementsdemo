package demo.jliu.elementsdemo.net;

import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import demo.jliu.elementsdemo.mode.Item;

public class Decoder {

    public static final String PATTERN_QUATED_STRING = "(\"([^\"]*\\n?)+\")+";
    private static final String PLACEHOLDER = "%s";
    private static final String TAG = "Decoder";
    private String rawText;
    private final ArrayList<Item> result = new ArrayList<>();
    private final ArrayList<String> invalidLines = new ArrayList<>();
    private final Queue<String> quatedStringQueue = new LinkedList<>();
    private boolean isDecoded;

    public Decoder(String rawText) {
        if (TextUtils.isEmpty(rawText)) {
            throw new IllegalArgumentException("empty text is not supported");
        }
        this.rawText = rawText;
    }

    public ArrayList<Item> getResult() {
        ensureStatus();
        return result;
    }

    public boolean hasErrors() {
        ensureStatus();
        return !invalidLines.isEmpty();
    }

    public ArrayList<String> getInvalidLines() {
        ensureStatus();
        return invalidLines;
    }

    private void ensureStatus() {
        if (!isDecoded) {
            throw new IllegalStateException("Call decode() first");
        }
    }

    public void decode() {
        if (isDecoded) {
            throw new IllegalStateException("Don't decode more than one time. To decode a new text, create a new instance");
        }
        Pattern pattern = Pattern.compile(PATTERN_QUATED_STRING);
        Matcher matcher = pattern.matcher(rawText);
        while (matcher.find()) {
            quatedStringQueue.add(matcher.group());
        }

        rawText = rawText.replaceAll(PATTERN_QUATED_STRING, PLACEHOLDER);

        Scanner scanner = new Scanner(rawText);
        scanner.nextLine();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            try {
                validate(line);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Invalid line: " + line);
                invalidLines.add(line);
                continue;
            }
            String titleRaw = line.substring(0, line.indexOf(","));
            String title = recover(titleRaw);
            line = line.substring(line.indexOf(",") + 1);

            String descRaw = line.substring(0, line.indexOf(","));
            String desc = recover(descRaw);
            line = line.substring(line.indexOf(",") + 1);

            String imageUrl = recover(line);
            result.add(new Item(title, desc, imageUrl));
        }
        isDecoded = true;
    }

    private void validate(String line) throws IllegalArgumentException {
        int comaCount = 0;
        for (char c : line.toCharArray()) {
            if (c == ',') {
                comaCount++;
            }
        }
        if (comaCount != 2) {
            throw new IllegalArgumentException("Invalid line:" + line);
        }
    }

    private String recover(String str) {
        while (str.contains(PLACEHOLDER)) {
            str = str.replace(PLACEHOLDER, quatedStringQueue.poll());
        }
        return str;
    }
}
